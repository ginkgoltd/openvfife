# GinkGo openVFIFE

## Description
The openVFIFE is a `C++` library for structural nonliear dynamic analysis and discontinuous behaviour analysis based on vector form intrinsic finite element method (referred to as VFIFE).  

**Motivation: ** The VFIFE has already been applied in many research fields, and has made great achievements. However, there is no available software/platform for it, which is an main obstcale of the development and popularization of VFIFE.  On the other hand, the author (Tan Biao or GinkGo) tries to introduce VFIFE into the collapse analysis of transmission tower. For this reason, the *openVFIFE* has been implemented to provide a general VFIFE analysis software/platform for researchers or engineers who interested in VFIFE.

**Final goal: ** The openVFIFE is currently released in the form of `C++` library. But it will never stop here. A completely independent cross-platform software package is my ultimate goal. The author will continue to work hard for this. And the contributions from all of you are quite important. If you have any suggestions or advices, or want to contribute new codes , please contact me (ginkgoltd@outlook.com).

**Main Characters: **

- openVFIFE is a completely open source library, which strictly abides by GNU/GPL v3.0 License (see LICENSE file).
- openVFIFE is fast.
- openVFIFE is reliable.
- openVFIFE is user- and developer-friendly.
- openVFIFE is scalable.

## Requirements

The `C++` standard library and `Eigen3` library (already contained in the source code) are need.

## Installation

```shell
$ g++ -c -O3 element.cpp gridsection.cpp integrator.cpp material.cpp particle.cpp section.cpp structsystem.cpp 
$ ar rcs libvfife.a element.o gridsection.o integrator.o material.o particle.o section.o structsystem.o
```

## Instructions

see the `./docs/user_maual.md` and `./docs/developer_manual.md`. 

For online documentations, please visit https://ginkgoltd.github.io. 

## License

openVFIFE is [Free Software](http://www.gnu.org/philosophy/free-sw.html). Starting from the 1.0 version, it is licensed under the GNU/GPL v3.0.

Make sure you are aware of the `COPYRITHT` and `LICENSE`.  **CAUTION:**

- This software program and documentation are copyrighted by GinkGo(Tan Biao). 
- The GinkGo does not warrant that the operation of the program will be uninterrupted or error-free. The end-user understands that the program was developed for research purposes and is advised not to rely exclusively on the program for any reason.

## Compiler support

openVFIFE is standard `C++11` and so should theoretically be compatible with any compliant compiler.Whenever we use some non-standard feature, that is optional and can be disabled.

Eigen is being successfully used with the following compilers:

- [GCC](http://gcc.gnu.org/), version 6.3 and newer. Older versions of gcc might work as well but they are not tested anymore.

More compilers such as MSVC(Visual Studio), Intel C++ compiler, LLVM/CLang++, Xcode, MinGW, and QNX's QCC compiler are not tested yet.  These compilers will be tested in future and the performance of these compilers will be compared as well.

## Bug reports

For bug reports and feature requests, please use the [issue tracker on GitHub](web site)

## Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request

## Mailing list

Address: ginkgoltd@outlook.com

The openVFIFE mailing list can be used for discussing general openVFIFE development topics. End-user questions are often better asked on the Users Forum. Development of specific features is best tracked and discussed on our issue tracker on GitHub.

This mailing list is public and has public archives.


## Credits
The openVFIFE project was started by Tan Biao (founder). Contribute your talents to help make openVFIFE successful. Here's an alphabetical list: (note to contributors: do add yourself!)

- Tan Biao (GinkGo) - founder of the project, and the first version of framework and implementation of the openVFIFE
- Looking forward to your joining!

**openVFIFE is also using code that we copied from other sources.** They are acknowledged in our sources and in the Mercurial history, but let's also mention them here:

- Eigen3 - [Eigen](http://eigen.tuxfamily.org/index.php?title=Main_Page) is a C++ template library for linear algebra: matrices, vectors, numerical solvers, and related algorithms.
