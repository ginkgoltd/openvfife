#include "../headers/gridsection.h"
#include <iostream>

using namespace std;




int main()
{
    double x = 5, length = 10;
    double s = x / length;
    SectionResponse response;
    BaseMaterial* mat = new UniBilinear(1, 1000, 100, 20, 0);
    BaseSection* rect = new Rectangle(1, 20, 20);
    GridRectangle grid = GridRectangle(2);
    grid.initiateResponseContainers(rect, &response);

    cout << "m1" << endl;
    Eigen::RowVectorXd transfer(5);
    transfer << 1, 6 * s - 4, 4 - 6 * s, 6 * s - 2, 2 - 6 * s;
    cout << transfer << endl;
    cout << "m2" << endl;


    Eigen::VectorXd deform(5);
    deform << 1e-6, 0, 3e-6, 0, 5e-6;

    cout << "m3" << endl;

    // vector<double> val;
    grid.force(mat, rect, transfer, deform, &response);


    cout << "m4" << endl;

    for (int i = 0; i < response.force.size(); i++)
    {
        cout << response.force[i] << ", ";
    }
    cout << endl;


    // cout << "m5" << endl;
    // SectionResponse response1;
    // GridRectangle grid2 = GridRectangle(2, &mat, 2);
    // grid2.setSection(&rect);
    // cout << "m6" << endl;
    // grid2.initiateResponseContainers(response1);
    // grid2.info();
    // grid2.force(transfer, deform, response1);
    // for (int i = 0; i < response1.force.size(); i++)
    // {
    //     cout << response1.force[i] << ", ";
    // }
    // cout << endl;


    cout << "--------------------------------------------------" << endl;
    SectionResponse res;
    GridSection* grid3 = CreateGridSection::getInstance(rect, 2);
    grid3->initiateResponseContainers(rect, &res);

    grid3->force(mat, rect, transfer, deform, &res);

    for (int i = 0; i < res.force.size(); i++)
    {
        cout << res.force[i] << ", ";
    }
    cout << endl;

    delete grid3;
    delete mat;
    delete rect;

    BaseSection* angle = new AngleSteel(1, 10, 10, 1, 1);
    GridSection* grid4 = CreateGridSection::getInstance(angle, 2, 4);


    delete angle;
    delete grid4;

}