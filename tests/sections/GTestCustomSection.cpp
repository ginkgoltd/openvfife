#include <gtest/gtest.h>
#include <gmock/gmock.h>

#define private public
#define protected public
#include <section.h>
#undef private
#undef protected

using namespace std;
using ::testing::ContainerEq;


class CustomSectionTest: public::testing::Test
{
    protected:
        // Declares the CustomSections
        CustomSectionParas para1_ = {.A=0.0, .Sy=0.0, .Sz=0.0, .SHy=0.0,
            .SHz=0.0, .CGy=0.0, .CGz=0.0, .Iyy=0.0, .Izz=0.0, .Iyz=0.0};
        CustomSectionParas para2_ = {.A=10.0, .Sy=0.0, .Sz=0.0, .SHy=0.0,
            .SHz=0.0, .CGy=0.0, .CGz=0.0, .Iyy=10.0, .Izz=10.0, .Iyz=0.0};
        CustomSection sect1_ = CustomSection(1, para1_);
        CustomSection sect2_ = CustomSection(2, para2_);
        CustomSection sect3_ = CustomSection(3, para1_);
        CustomSection sect4_ = CustomSection(4, para1_);

        // other data

        // You can remove any or all of the following functions if their bodies
        // would be empty.
        CustomSectionTest()
        {
            // You can do set-up work for each test here.
        };

        ~CustomSectionTest()
        {
            // You can do clean-up work that doesn't throw exceptions here.
        };

        // If the constructor and destructor are not enough for setting up
        // and cleaning up each test, you can define the following methods:
        void SetUp() override
        {
            // Code here will be called immediately after the constructor
            // (right before each test).
            cout << "Set Up CustomSection Test" << endl;
        };

        void TearDown() override
        {
            // Code here will be called immediately after each test (right
            // before the destructor).
        };

        // Class members declared here can be used by all tests in the test
        // suite for Foo.
};


// Tests that the CustomSection::CustomSection() method does Abc.
TEST_F(CustomSectionTest, IsEmptyInitiate)
{
    cout << "\n---------- CustomSectionTest IsEmptyInitiate ----------" << endl;
    cout << "check id... " << endl;
    EXPECT_EQ(sect1_.id(), 1);
    EXPECT_EQ(sect2_.id(), 2);
    EXPECT_EQ(sect3_.id(), 3);
    EXPECT_EQ(sect4_.id(), 4);

    cout << "check type... " << endl;
    char type[] = "CustomSection";
    EXPECT_STREQ(type, sect1_.type_.c_str());

    cout << "check A... " << endl;
    EXPECT_DOUBLE_EQ(0.0, sect1_.A_);
    EXPECT_DOUBLE_EQ(10.0, sect2_.A_);
    EXPECT_DOUBLE_EQ(0.0, sect3_.A_);
    EXPECT_DOUBLE_EQ(0.0, sect4_.A_);


    cout << "check Sy... " << endl;
    EXPECT_DOUBLE_EQ(0.0, sect1_.Sy_);
    EXPECT_DOUBLE_EQ(0.0, sect2_.Sy_);
    EXPECT_DOUBLE_EQ(0.0, sect3_.Sy_);
    EXPECT_DOUBLE_EQ(0.0, sect4_.Sy_);

    cout << "check Sz... " << endl;
    EXPECT_DOUBLE_EQ(0.0, sect1_.Sz_);
    EXPECT_DOUBLE_EQ(0.0, sect2_.Sz_);
    EXPECT_DOUBLE_EQ(0.0, sect3_.Sz_);
    EXPECT_DOUBLE_EQ(0.0, sect4_.Sz_);

    cout << "check CGy... " << endl;
    EXPECT_DOUBLE_EQ(0.0, sect1_.CGy_);
    EXPECT_DOUBLE_EQ(0.0, sect2_.CGy_);
    EXPECT_DOUBLE_EQ(0.0, sect3_.CGy_);
    EXPECT_DOUBLE_EQ(0.0, sect4_.CGy_);

    cout << "check CGz... " << endl;
    EXPECT_DOUBLE_EQ(0.0, sect1_.CGz_);
    EXPECT_DOUBLE_EQ(0.0, sect2_.CGz_);
    EXPECT_DOUBLE_EQ(0.0, sect3_.CGz_);
    EXPECT_DOUBLE_EQ(0.0, sect4_.CGz_);

    cout << "check SHy... " << endl;
    EXPECT_DOUBLE_EQ(0.0, sect1_.SHy_);
    EXPECT_DOUBLE_EQ(0.0, sect2_.SHy_);
    EXPECT_DOUBLE_EQ(0.0, sect3_.SHy_);
    EXPECT_DOUBLE_EQ(0.0, sect4_.SHy_);

    cout << "check SHz... " << endl;
    EXPECT_DOUBLE_EQ(0.0, sect1_.SHz_);
    EXPECT_DOUBLE_EQ(0.0, sect2_.SHz_);
    EXPECT_DOUBLE_EQ(0.0, sect3_.SHz_);
    EXPECT_DOUBLE_EQ(0.0, sect4_.SHz_);

    cout << "check Iyy_... " << endl;
    EXPECT_DOUBLE_EQ(0.0, sect1_.Iyy_);
    EXPECT_DOUBLE_EQ(10.0, sect2_.Iyy_);
    EXPECT_DOUBLE_EQ(0.0, sect3_.Iyy_);
    EXPECT_DOUBLE_EQ(0.0, sect4_.Iyy_);

    cout << "check Izz_... " << endl;
    EXPECT_DOUBLE_EQ(0.0, sect1_.Izz_);
    EXPECT_DOUBLE_EQ(10.0, sect2_.Izz_);
    EXPECT_DOUBLE_EQ(0.0, sect3_.Izz_);
    EXPECT_DOUBLE_EQ(0.0, sect4_.Izz_);

    cout << "check Iyz_... " << endl;
    EXPECT_DOUBLE_EQ(0.0, sect1_.Iyz_);
    EXPECT_DOUBLE_EQ(0.0, sect2_.Iyz_);
    EXPECT_DOUBLE_EQ(0.0, sect3_.Iyz_);
    EXPECT_DOUBLE_EQ(0.0, sect4_.Iyz_);
};


// Tests that the CustomSection::calcArea() method.
TEST_F(CustomSectionTest, calcArea)
{
    cout << "\n---------- CustomSectionTest calcArea ----------" << endl;
    cout << "do nothing..." << endl;
    sect1_.calcArea();
};


// Tests that the CustomSection::calcSy() method.
TEST_F(CustomSectionTest, calcSy)
{
    cout << "\n---------- CustomSectionTest calcSy ----------" << endl;
    cout << "do nothing..." << endl;
    sect1_.calcSy();
};


// Tests that the CustomSection::calcSz() method does Abc.
TEST_F(CustomSectionTest, calcSz)
{
    cout << "\n---------- CustomSectionTest calcSz ----------" << endl;
    cout << "do nothing..." << endl;
    sect1_.calcSz();
};


// Tests that the CustomSection::calcCGz() method does Abc.
TEST_F(CustomSectionTest, calcCGz)
{
    cout << "\n---------- CustomSectionTest calcCGz ----------" << endl;
    cout << "do nothing..." << endl;
    sect1_.calcCGz();
};


// Tests that the CustomSection::calcSHy() method does Abc.
TEST_F(CustomSectionTest, calcSHy)
{
    cout << "\n---------- CustomSectionTest calcSHy ----------" << endl;
    cout << "do nothing..." << endl;
    sect1_.calcSHy();
};


// Tests that the CustomSection::calcSHz() method does Abc.
TEST_F(CustomSectionTest, calcSHz)
{
    cout << "\n---------- CustomSectionTest calcSHz ----------" << endl;
    cout << "do nothing..." << endl;
    sect1_.calcSHz();
};


// Tests that the CustomSection::calcIyy() method does Abc.
TEST_F(CustomSectionTest, calcIyy)
{
    cout << "\n---------- CustomSectionTest calcIyy ----------" << endl;
    cout << "do nothing..." << endl;
    sect1_.calcIyy();
};


// Tests that the CustomSection::calcIzz() method does Abc.
TEST_F(CustomSectionTest, calcIzz)
{
    cout << "\n---------- CustomSectionTest calcIzz ----------" << endl;
    cout << "do nothing..." << endl;
    sect1_.calcIzz();
};


// Tests that the CustomSection::calcIyz() method does Abc.
TEST_F(CustomSectionTest, calcIyz)
{
    cout << "\n---------- CustomSectionTest calcIyz ----------" << endl;
    cout << "do nothing..." << endl;
    sect1_.calcIyz();
};


// Tests that the CustomSection::info() method does Abc.
TEST_F(CustomSectionTest, info)
{
    // do not need to test
};
