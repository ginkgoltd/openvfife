#include <gtest/gtest.h>
#include <gmock/gmock.h>

#define private public
#define protected public
#include <section.h>
#undef private
#undef protected

using namespace std;
using ::testing::ContainerEq;


class CtubeTest: public::testing::Test
{
    protected:
        // Declares the Ctubes
        double ri_ = 1.0, ro_ = 2.0;

        Ctube sect1_ = Ctube(1, ri_, ro_);
        // Ctube sect2_ = Ctube(2, 1.0, 0.0);

        // other data

        // You can remove any or all of the following functions if their bodies
        // would be empty.
        CtubeTest()
        {
            // You can do set-up work for each test here.
        };

        ~CtubeTest()
        {
            // You can do clean-up work that doesn't throw exceptions here.
        };

        // If the constructor and destructor are not enough for setting up
        // and cleaning up each test, you can define the following methods:
        void SetUp() override
        {
            // Code here will be called immediately after the constructor
            // (right before each test).
            cout << "Set Up Ctube Test" << endl;
        };

        void TearDown() override
        {
            // Code here will be called immediately after each test (right
            // before the destructor).
        };

        // Class members declared here can be used by all tests in the test
        // suite for Foo.
};


// Tests that the Ctube::Ctube() method does Abc.
TEST_F(CtubeTest, IsEmptyInitiate)
{
    cout << "\n---------- CtubeTest IsEmptyInitiate ----------" << endl;
    cout << "check id... " << endl;
    EXPECT_EQ(sect1_.id(), 1);
    // EXPECT_EQ(sect2_.id(), 2);

    cout << "check type... " << endl;
    char type[] = "Ctube";
    EXPECT_STREQ(type, sect1_.type_.c_str());

    cout << "check A... " << endl;
    double area = PI * (ro_ * ro_ - ri_ * ri_);
    EXPECT_DOUBLE_EQ(area, sect1_.A_);
    // EXPECT_DOUBLE_EQ(0.0, sect2_.A_);

    cout << "check Sy... " << endl;
    EXPECT_DOUBLE_EQ(0.0, sect1_.Sy_);

    cout << "check Sz... " << endl;
    EXPECT_DOUBLE_EQ(0.0, sect1_.Sz_);

    cout << "check CGy... " << endl;
    EXPECT_DOUBLE_EQ(0.0, sect1_.CGy_);

    cout << "check CGz... " << endl;
    EXPECT_DOUBLE_EQ(0.0, sect1_.CGz_);

    cout << "check SHy... " << endl;
    EXPECT_DOUBLE_EQ(0.0, sect1_.SHy_);

    cout << "check SHz... " << endl;
    EXPECT_DOUBLE_EQ(0.0, sect1_.SHz_);

    cout << "check Iyy_... " << endl;
    double Iyy = PI / 64.0 * (pow(2 * ro_, 4) - pow(2 * ri_, 4));
    EXPECT_DOUBLE_EQ(Iyy, sect1_.Iyy_);

    cout << "check Izz_... " << endl;
    double Izz = PI / 64.0 * (pow(2 * ro_, 4) - pow(2 * ri_, 4));
    EXPECT_DOUBLE_EQ(Izz, sect1_.Izz_);

    cout << "check Iyz_... " << endl;
    EXPECT_DOUBLE_EQ(0.0, sect1_.Iyz_);
};


// Tests that the Ctube::calcArea() method.
TEST_F(CtubeTest, A)
{
    cout << "\n---------- CtubeTest A ----------" << endl;
    cout << "check A..." << endl;
    double area = PI * (ro_ * ro_ - ri_ * ri_);
    EXPECT_DOUBLE_EQ(area, sect1_.A());
};


// Tests that the Ctube::Sy() method.
TEST_F(CtubeTest, Sy)
{
    cout << "\n---------- CtubeTest calcSy ----------" << endl;
    cout << "do nothing..." << endl;
    EXPECT_DOUBLE_EQ(0.0, sect1_.Sy());
};


// Tests that the Ctube::Sz() method does Abc.
TEST_F(CtubeTest, Sz)
{
    cout << "\n---------- CtubeTest Sz ----------" << endl;
    cout << "do nothing..." << endl;
    EXPECT_DOUBLE_EQ(0.0, sect1_.Sz());
};


// Tests that the Ctube::calcCGz() method does Abc.
TEST_F(CtubeTest, CGz)
{
    cout << "\n---------- CtubeTest CGz ----------" << endl;
    cout << "do nothing..." << endl;
    EXPECT_DOUBLE_EQ(0.0, sect1_.CGz());
};


// Tests that the Ctube::SHy() method does Abc.
TEST_F(CtubeTest, SHy)
{
    cout << "\n---------- CtubeTest SHy ----------" << endl;
    cout << "do nothing..." << endl;
    EXPECT_DOUBLE_EQ(0.0, sect1_.SHy());
};


// Tests that the Ctube::SHz() method does Abc.
TEST_F(CtubeTest, SHz)
{
    cout << "\n---------- CtubeTest SHz ----------" << endl;
    cout << "do nothing..." << endl;
    EXPECT_DOUBLE_EQ(0.0, sect1_.SHz());
};


// Tests that the Ctube::Iyy() method does Abc.
TEST_F(CtubeTest, Iyy)
{
    cout << "\n---------- CtubeTest Iyy ----------" << endl;
    cout << "do nothing..." << endl;
    double Iyy = PI / 64.0 * (pow(2 * ro_, 4) - pow(2 * ri_, 4));
    EXPECT_DOUBLE_EQ(Iyy, sect1_.Iyy());
};


// Tests that the Ctube::calcIzz() method does Abc.
TEST_F(CtubeTest, calcIzz)
{
    cout << "\n---------- CtubeTest calcIzz ----------" << endl;
    cout << "do nothing..." << endl;
    double Izz = PI / 64.0 * (pow(2 * ro_, 4) - pow(2 * ri_, 4));
    EXPECT_DOUBLE_EQ(Izz, sect1_.Izz());
};


// Tests that the Ctube::calcIyz() method does Abc.
TEST_F(CtubeTest, Iyz)
{
    cout << "\n---------- CtubeTest calcIyz ----------" << endl;
    cout << "do nothing..." << endl;
    EXPECT_DOUBLE_EQ(0.0, sect1_.Iyz());
};


// Tests that the Ctube::info() method does Abc.
TEST_F(CtubeTest, info)
{
    // do not need to test
};
