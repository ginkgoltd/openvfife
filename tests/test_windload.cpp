#include "../headers/windload.h"

using namespace std;
using namespace load;

int main()
{
    {
        cout << "\n----- test WindVelocity -----" << endl;
        WindVelocity w1 = {.u=10, .v=20, .w=15};
        cout << "u: " << w1.u << ", v: " << w1.v << ", w: " << w1.w << endl;
    }

    {
        cout << "\n----- test Wind -----" << endl;
        Wind w2 = Wind(10);
        cout << "w2: " << endl;
        cout << "speed before: " << w2.speed() << endl;
        w2.setSpeed(15);
        cout << "speed after: " << w2.speed() << endl;
    }

}