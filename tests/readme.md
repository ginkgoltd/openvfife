# Unittests of openVFIFE

This file records the unit tests progress of each module of openVFIFE.

**WARNING**: I am struggling for my doctoral thesis, which makes me miserable. So all tests are quite simple, and incomplete.  I know unit testing is important to ensure the reliability of openVFIFE, and it is also the basis of maintenance, extension and refatoring. A unit testing framework(listed here) will be applied, once I graduate.  

- Microsoft Unit Testing Framework for C++

- Google Test

- Boost.Test (most favorable)

- CTest

  

## Particle class

$ m \ddot{x} + c \dot{x} + k x = f ​$

- [x] only tested several get/set method

## Material Module

- [x] tested `Eq()` function of `UniBlinear` and `UniIdeal` class, and **Failed** in `UniPower` class

## Section Module

- [x] `CustomSection`
- [x] `Ctube`
- [x] `AngleSteel`
- [ ] `Rectangle`

## GridSection Module

- [ ] Test Nothing

## Integrator Module

- [ ] Test Nothing



## Element Module

- [ ] Test Nothing. 
- [ ] 

## StructSystem class

- [ ] Test several methods.

**Actually I have wrote several tests for the rest part, unfortuately I delete them by accident. I know they are right, but I can not illustrate it for you guys. I am tired, I will not write new tests in the near future. Damn it!!!!!** 