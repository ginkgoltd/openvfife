#include <gtest/gtest.h>
#include <gmock/gmock.h>

#define private public
#define protected public
#include <material.h>
#undef private
#undef protected

using namespace std;
using ::testing::ContainerEq;


class UniBilinearTest: public::testing::Test
{
    protected:
        // Declares the UniBilinears
        UniBilinear mat1_ = UniBilinear(1, 0, 0, 0, 0);
        UniBilinear mat2_ = UniBilinear(2, 1000, 100, 20, 0);
        UniBilinear mat3_ = UniBilinear(3, 0, 0, 0, 0);
        UniBilinear mat4_ = UniBilinear(4, 0, 0, 0, 0);

        // other data
        double stress = 0, strain = 0, ps = 0, alpha = 0, q = 0;

        // You can remove any or all of the following functions if their bodies
        // would be empty.
        UniBilinearTest()
        {
            // You can do set-up work for each test here.
        };

        ~UniBilinearTest()
        {
            // You can do clean-up work that doesn't throw exceptions here.
        };

        // If the constructor and destructor are not enough for setting up
        // and cleaning up each test, you can define the following methods:
        void SetUp() override
        {
            // Code here will be called immediately after the constructor
            // (right before each test).
            cout << "Set Up UniBilinear Test" << endl;
        };

        void TearDown() override
        {
            // Code here will be called immediately after each test (right
            // before the destructor).
        };

        // Class members declared here can be used by all tests in the test
        // suite for Foo.
};


// Tests that the UniBilinear::UniBilinear() method does Abc.
TEST_F(UniBilinearTest, IsEmptyInitiate)
{
    cout << "\n---------- UniBilinearTest IsEmptyInitiate ----------" << endl;
    cout << "check id... " << endl;
    EXPECT_EQ(mat1_.id(), 1);
    EXPECT_EQ(mat2_.id(), 2);
    EXPECT_EQ(mat3_.id(), 3);
    EXPECT_EQ(mat4_.id(), 4);

    cout << "check type... " << endl;
    char type[] = "UniBilinear";
    EXPECT_STREQ(type, mat1_.type_.c_str());

    cout << "check E... " << endl;
    EXPECT_DOUBLE_EQ(0.0, mat1_.E_);

    cout << "check G... " << endl;
    EXPECT_DOUBLE_EQ(0.0, mat1_.G_);

    cout << "check nu... " << endl;
    EXPECT_DOUBLE_EQ(0.0, mat1_.nu_);

    cout << "check density... " << endl;
    EXPECT_DOUBLE_EQ(0.0, mat1_.density_);

    cout << "check ce... " << endl;
    EXPECT_DOUBLE_EQ(0.0, mat1_.ce_);

    cout << "check damp_ratio... " << endl;
    EXPECT_DOUBLE_EQ(0.0, mat1_.damp_ratio_);

    cout << "check damp_coef... " << endl;
    EXPECT_DOUBLE_EQ(0.0, mat1_.damp_coef_);

    cout << "check mu... " << endl;
    EXPECT_DOUBLE_EQ(0.0, mat1_.mu_);

    cout << "check yield_stress... " << endl;
    EXPECT_DOUBLE_EQ(0.0, mat1_.yield_stress_);

    cout << "check limited_tensile_strain... " << endl;
    EXPECT_DOUBLE_EQ(9.99e99, mat1_.limited_tensile_strain_);

    cout << "check limited_compress_strain... " << endl;
    EXPECT_DOUBLE_EQ(-9.99e99, mat1_.limited_compress_strain_);

    cout << "check m... " << endl;
    EXPECT_DOUBLE_EQ(0, mat1_.m_);

    cout << "check Et... " << endl;
    EXPECT_DOUBLE_EQ(0, mat1_.Et_);
    EXPECT_DOUBLE_EQ(100, mat2_.Et_);
};


// Tests that the UniBilinear::isFracture() method.
TEST_F(UniBilinearTest, isFracture)
{
    cout << "\n---------- UniBilinearTest isFracture ----------" << endl;
    double val1 = 0.03, val2 = -0.04;
    mat1_.setLimitedStrain(val1, val2);
    EXPECT_FALSE(mat1_.isFractured(0.0));
    EXPECT_FALSE(mat1_.isFractured(0.01));
    EXPECT_FALSE(mat1_.isFractured(-0.01));

    EXPECT_TRUE(mat1_.isFractured(0.03));
    EXPECT_TRUE(mat1_.isFractured(-0.04));

    EXPECT_TRUE(mat1_.isFractured(0.05));
    EXPECT_TRUE(mat1_.isFractured(-0.04));
};


// Tests that the UniBilinear::setE() method.
TEST_F(UniBilinearTest, setE)
{
    cout << "\n---------- UniBilinearTest setE ----------" << endl;
    double val = 2000;
    mat1_.setE(val);
    EXPECT_DOUBLE_EQ(val, mat1_.E_);

    // input an negative value
    val = -100;
    mat1_.setE(val);
    EXPECT_DOUBLE_EQ(val, mat1_.E_);
};


// Tests that the UniBilinear::setG() method does Abc.
TEST_F(UniBilinearTest, setG)
{
    cout << "\n---------- UniBilinearTest setG ----------" << endl;
    double val = 2000;
    mat1_.setG(val);
    EXPECT_DOUBLE_EQ(val, mat1_.G_);

    // input an negative value
    val = -100;
    mat1_.setG(val);
    EXPECT_DOUBLE_EQ(val, mat1_.G_);
};


// Tests that the UniBilinear::setNu() method does Abc.
TEST_F(UniBilinearTest, setNu)
{
    cout << "\n---------- UniBilinearTest setNu ----------" << endl;
    double val = 2000;
    mat1_.setNu(val);
    EXPECT_DOUBLE_EQ(val, mat1_.nu_);
    double G = mat1_.E_ / 2.0 / (1 + val);
    EXPECT_DOUBLE_EQ(G, mat1_.G_);

    // input an negative value
    val = -100;
    mat1_.setNu(val);
    EXPECT_DOUBLE_EQ(val, mat1_.nu_);
    G = mat1_.E_ / 2.0 / (1 + val);
    EXPECT_DOUBLE_EQ(G, mat1_.G_);
};


// Tests that the UniBilinear::setDensity() method does Abc.
TEST_F(UniBilinearTest, setDensity)
{
    cout << "\n---------- UniBilinearTest setDensity ----------" << endl;
    double val = 2000;
    mat1_.setDensity(val);
    EXPECT_DOUBLE_EQ(val, mat1_.density_);

    // input an negative value
    val = -100;
    mat1_.setDensity(val);
    EXPECT_DOUBLE_EQ(val, mat1_.density_);
};


// Tests that the UniBilinear::calcCe() method does Abc.
TEST_F(UniBilinearTest, calcCe)
{
    cout << "\n---------- UniBilinearTest calcCe ----------" << endl;
    // EXPECT_DOUBLE_EQ(0.0/0.0, mat1_.calcCe());
    double E = 2000, density = 20;
    mat1_.setE(E);
    mat1_.setDensity(density);
    EXPECT_DOUBLE_EQ(sqrt(E/density), mat1_.calcCe());
};


// Tests that the UniBilinear::setDampRatio() method does Abc.
TEST_F(UniBilinearTest, setDampRatio)
{
    cout << "\n---------- UniBilinearTest setDampRatio ----------" << endl;
    double val = 2000;
    mat1_.setDampRatio(val);
    EXPECT_DOUBLE_EQ(val, mat1_.damp_ratio_);

    // input an negative value
    val = -100;
    mat1_.setDampRatio(val);
    EXPECT_DOUBLE_EQ(val, mat1_.damp_ratio_);
};


// Tests that the UniBilinear::setDampCoef() method does Abc.
TEST_F(UniBilinearTest, setDampCoef)
{
    cout << "\n---------- UniBilinearTest setDampCoef ----------" << endl;
    double val = 2000;
    mat1_.setDampCoef(val);
    EXPECT_DOUBLE_EQ(val, mat1_.damp_coef_);

    // input an negative value
    val = -100;
    mat1_.setDampCoef(val);
    EXPECT_DOUBLE_EQ(val, mat1_.damp_coef_);
};


// Tests that the UniBilinear::setMu() method does Abc.
TEST_F(UniBilinearTest, setMu)
{
    cout << "\n---------- UniBilinearTest setMu ----------" << endl;
    double val = 2000;
    mat1_.setMu(val);
    EXPECT_DOUBLE_EQ(val, mat1_.mu_);

    // input an negative value
    val = -100;
    mat1_.setMu(val);
    EXPECT_DOUBLE_EQ(val, mat1_.mu_);
};


// Tests that the UniBilinear::setLimitedTensileStrain() method does Abc.
TEST_F(UniBilinearTest, setLimitedTensileStrain)
{
    cout << "\n---------- UniBilinearTest setLimitedTensileStrain ----------" << endl;
    double val = 2000;
    mat2_.setLimitedTensileStrain(val);
    EXPECT_DOUBLE_EQ(val, mat2_.limited_tensile_strain_);

    // input an negative value
    val = -100;
    mat1_.setLimitedTensileStrain(val);
    EXPECT_DOUBLE_EQ(9.99e99, mat1_.limited_tensile_strain_);

};


// Tests that the UniBilinear::setLimitedCompressStrain() method does Abc.
TEST_F(UniBilinearTest, setLimitedCompressStrain)
{
    cout << "\n---------- UniBilinearTest setLimitedCompressStrain ----------" << endl;
    double val = 2000;
    mat1_.setLimitedCompressStrain(val);
    EXPECT_DOUBLE_EQ(-9.99e99, mat1_.limited_compress_strain_);

    // input an negative value
    val = -100;
    mat2_.setLimitedCompressStrain(val);
    EXPECT_DOUBLE_EQ(val, mat2_.limited_compress_strain_);
};


// Tests that the UniBilinear::setLimitedStrain() method does Abc.
TEST_F(UniBilinearTest, setLimitedStrain)
{
    cout << "\n---------- UniBilinearTest setLimitedStrain ----------" << endl;
    double val1 = 1000, val2 = -1000;
    mat1_.setLimitedStrain(val1, val1);
    EXPECT_DOUBLE_EQ(val1, mat1_.limited_tensile_strain_);
    EXPECT_DOUBLE_EQ(-9.99e99, mat1_.limited_compress_strain_);

    mat2_.setLimitedStrain(val1, val2);
    EXPECT_DOUBLE_EQ(val1, mat2_.limited_tensile_strain_);
    EXPECT_DOUBLE_EQ(val2, mat2_.limited_compress_strain_);

    mat3_.setLimitedStrain(val2, val1);
    EXPECT_DOUBLE_EQ(9.99e99, mat3_.limited_tensile_strain_);
    EXPECT_DOUBLE_EQ(-9.99e99, mat3_.limited_compress_strain_);

    mat4_.setLimitedStrain(val2, val2);
    EXPECT_DOUBLE_EQ(9.99e99, mat4_.limited_tensile_strain_);
    EXPECT_DOUBLE_EQ(val2, mat4_.limited_compress_strain_);
};


// Tests that the UniBilinear::Eq() method does Abc.
TEST_F(UniBilinearTest, Eq)
{
// hard to evaluate, test_unibilinear_eq.cpp has been writen to test it by
// comparing with theory results
};


// Tests that the UniBilinear::info() method does Abc.
TEST_F(UniBilinearTest, info)
{
    // do not need to test
};
