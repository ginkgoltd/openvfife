# Code Conventions 编码规范

参考Google C++ Style Guide

## 1. Naming Conventions 命名规范

### 1.1 通用命名规则

尽可能给有描述性的命名，别心疼空间，毕竟让代码易于新读者理解很重要。不要用只有项目开发者能理解的缩
写，也不要通过砍掉几个字母来缩写单词。

```c++
// good
int price_count_reader;
int num_errors;
int num_dns_connections;

// terrible
int n;
int nerr;
int n_comp_conns;
int wgc_connections;
int pc_readers;
int cstmr_id;
```

### 1.2 文件命名

文件名要全部小写, 可以包含下划线 (_) 或连字符 (-). 按项目约定来. 如果并没有项目约定， ”_” 更好。
可接受的文件命名:

```shell
my_useful_class.cpp
```

### 1.3 类型命名

**Tip:** 类型名称的每个单词首字母均大写, 不包含下划线: MyExcitingClass, MyExcitingEnum.

所有类型命名——类, 结构体, 类型定义 (typedef), 枚举——均使用相同约定. 例如:

```c++
// classes and structs
class UrlTable { ... };
class UrlTableTester { ... };
struct UrlTableProperties { ... };

// typedefs
typedef hash_map<UrlTableProperties *, string> PropertiesMap;

// enums
enum UrlTableErrors { ... };

```

### 1.4 变量命名

**Tip: **变量名一律小写，单词之间用下划线连接。类的成员变量以下划线结尾，但结构体的就不用，如：`a_local_variable`, `a_struct_data_member`, `a_class_data_member_`。

- 普通变量命名：

```c++
// good
string table_name;
string tablename;

// terrible
string tableName;
```

- 类数据成员

  不管是静态的还是非静态的，类数据成员都可以和普通变量一样，但是要接下划线

  ```c++
  class TableInfo
  {
  	private:
      	string table_name_;
      	string tablename_;
      	static Pool<TableInfo>* pool_;
  }
  ```

- 结构体变量

  不管是静态的还是非静态的，结构体数据成员都可以和普通变量一样，不用像类那样接下划线

  ```c++
  Struct UrlTableProperties
  {
  	string name;
  	int num_entries;
  }
  ```

- 全局变量

  对全局变量没有特别要求，少用就好，但如果要用，可以用`g_`标志作为前缀，一遍更好地区分局部变量

### 1.5 常量命名

**Tip: **在全局或类里的常量名称前加`k`，如`kDaysInAWeek`，且除去开头的`k`之外每个单词开头字母均大写。所有编译时常量，无论是局部的，全局的还是类中的，和其他变量稍微区别一下。`k`后接大写字母开头的单词
```c++
const int kDaysInAWeek = 7;
```
这规则适用于编译时的局部作用域常量，不过要按变量规则来命名也可以。

### 1.6 函数命名

**Tip: **常规函数使用大小写混合, 取值和设值函数则要求与变量名匹配:`MyExcitingFunciton()`,`MyExcitingMethod()`, `my_exciting_memeber_variable()`,`set_my_exciting_member_variable()`

- 常规函数

  函数名的每个首字母大写，没有下划线

  如果您的某函数出错时就要直接 crash, 那么就在函数名加上 OrDie. 但这函数本身必须集成在产品代码
  里，且平时也可能会出错。

  ```c++
  AddTableEntry();
  DeleteUrl();
  OpenFileOrDie();
  ```

- 取值和设值函数

  取值（ Accessors）和设值（ Mutators）函数要与存取的变量名匹配. 这儿摘录一个类, `num_entries_ `是该
  类的实例变量:

  ```c++
  class MyClass
  {
      public:
      	...
          int num_entries() const {return num_entries_;};
      	void set_num_entries(int num_entries) {num_entries_ = num_entries;};
      
      private:
      	int num_entries_;
  }
  ```

  其它非常短小的内联函数名也可以用小写字母, 例如. 如果你在循环中调用这样的函数甚至都不用缓存其
  返回值, 小写命名就可以接受

### 1.7 名字空间命名

**Tip: **名字空间用小写字母命名, 并基于项目名称和目录结构: `google_awesome_project`

### 1.8 枚举命名

**Tip: **枚举的命名应当和常量或者宏一致：`kEnumName`或是`ENUM_NAME`

```c++
enum UrlTableErros
{
    kOK = 0,
    kErrorOutOfMemory,
    kErrorMalformedInput,
}
```

