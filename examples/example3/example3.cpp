#include "../../headers/structsystem.h"

using namespace std;


int main()
{
    // create system
    StructSystem system = StructSystem(1);
    system.setJobname("example3");

    // solve parameters
    double endTime = 50.0;
    double zeta = 0.5;
    double h = 0.01;
    double p = 100;

    // create particles
    Particle* p1 = new Particle(1, 0, 0);
    Particle* p2 = new Particle(2, 10, 0);
    Particle* p3 = new Particle(3, 10, 5);
    system.addParticle(p1);
    system.addParticle(p2);
    system.addParticle(p3);
    StdArray6d mass = {10.0, 10.0, 0, 0, 0, 0};
    p2->setLumpedMass(mass);

    // create material
    LinearElastic mat1 = LinearElastic(1);
    mat1.setE(2.0E4);
    LinearElastic mat2 = LinearElastic(2);
    mat2.setE(1.0E4);

    // create section
    CustomSectionParas paras = {.A=1, .Sy=0, .Sz=0, .SHy=0, .SHz=0,
                                .CGy=0, .CGz=0.0, .Iyy=10, .Izz=10, .Iyz=10};
    CustomSection sect = CustomSection(1, paras);

    // create elements
    Link2D* e1 = new Link2D(1, p1, p2, &mat1, &sect);
    Link2D* e2 = new Link2D(2, p2, p3, &mat2, &sect);
    system.addElement(e1);
    system.addElement(e2);

    // constraints
    DOF c1 = {.key = {true, true, true, true, true, true},
              .val = {0, 0, 0, 0, 0, 0}};
    DOF c3 = {.key = {true, true, true, true, true, true},
              .val = {0, 0, 0, 0, 0, 0}};
    system.addConstraint(1, c1);
    system.addConstraint(3, c3);

    // save model
    string path = system.workdir() + "/" + system.jobname() + "/model";
    system.saveModel(path);
    system.info();      // print structsystem information

    StdArray6d f1 = {0, -20, 0, 0, 0, 0};
    StdArray6d f2 = {20, -20, 0, 0, 0, 0};
    StdArray6d f3 = {20, 0, 0, 0, 0, 0};

    int nStep = ceil(endTime/h);
    cout << nStep << endl;
    int interval = ceil(0.1/h);
    for (int i = 0; i <= nStep; i++)
    {
        if (i == 0)
        {
            // add external force
            system.addExternalForce(1, f1);
            system.addExternalForce(2, f2);
            system.addExternalForce(3, f3);
            system.solve(h, zeta, true);
            system.setInternalForce();
        }
        else
        {
            system.solve(h, zeta);
            system.clearParticleForce();
            // add external force
            system.addExternalForce(1, f1);
            system.addExternalForce(2, f2);
            system.addExternalForce(3, f3);
            system.setInternalForce();
        }

        // save results
        if (i % interval == 0)
        {
            string path = system.workdir() + "/" + system.jobname() + "/" +
                          to_string(i*h);
            system.saveParticleResult(path);
            system.saveElementResult(path);
            system.saveSupportReact(path);
        }
    }

    system.releaseContainers();
    return 0;
}