import os
import numpy as np
import matplotlib.pyplot as plt

import sys

from numpy.lib.function_base import disp
sys.path.insert(0, '/media/tan/_dde_data/projects/ginkgo_vfife/scripts')
from visualization import ResultFileSystem, VtkWriter



if __name__ == "__main__":

    # load data from openVFIFE result, differenct damping coeff
    dir = "/home/tan/Desktop/test/vfife_examples/example4/"

    fname = os.path.join(dir, "example4")
    file = ResultFileSystem(fname)

    jobname = "example4"
    vtk = VtkWriter(file, jobname)
    descripts = "animation of example4"
    vtk.set_descripts(descripts)
    vtk.set_output_dir(dir)
    vtk.generate()
