#include "../../headers/structsystem.h"
using namespace std;

inline void printStdArray6d(const StdArray6d* arr)
{
    for (int i = 0; i < 6; i++)
    {
        cout << (*arr)[i] << ",";
    }
    cout << endl;
}


int main()
{
    // create system
    StructSystem system = StructSystem(1);
    system.setJobname("Example1");

    double zeta = 0.0;
    cout << "Please enter a damping coefficient(>=0): " << endl;
    cin >> zeta;

    // solve parameters
    double endTime = 10.0;  // total time, s
    double h = 1.0e-3;      // time step, s
    double v0 = 10;         // initial velocity, m/s
    double mass = 10;       // mass, kg
    double g = 9.8;         // acceleration of gravity, m/s-2

    // create particles
    Particle* p1 = new Particle(1, 0, 0);
    system.addParticle(p1);
    // set mass properties
    StdArray6d m = {mass, mass, 0, 0, 0, 0};
    p1->setLumpedMass(m);
    // activate dof of particle
    p1->activateDof("Ux");
    p1->activateDof("Uy");

    string path = system.workdir() + "/" + system.jobname() + "/model";
    system.saveModel(path);

    int nStep = ceil(endTime / h);
    int interval = ceil(0.1/h);
    for (int i = 0; i <= nStep; i++)
    {
        if (i == 0)
        {
            // set initial velocity and acclerate
            StdArray6d v = {v0, 0, 0, 0, 0, 0};
            p1->setVelocity(v);
            system.setAccelerate(0, g, 0);
            system.solve(h, zeta, true);
        }
        else
        {
            system.solve(h, zeta);
            system.clearParticleForce();
            system.setAccelerate(0, g, 0); // add external force
            system.setInternalForce();
        }

        // save results
        if (i % interval == 0)
        {
            string path = system.workdir() + "/" + system.jobname() + "/" +
                          to_string(i*h);
            system.saveParticleResult(path);
        }
    }

    system.releaseContainers();
    return 0;
}
